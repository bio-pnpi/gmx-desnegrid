/*
 * This file is part of the GROMACS molecular simulation package.
 *
 * Copyright (c) 2013,2014,2015, by the GROMACS development team, led by
 * Mark Abraham, David van der Spoel, Berk Hess, and Erik Lindahl,
 * and including many others, as listed in the AUTHORS file in the
 * top-level source directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */
/*! \internal \file
 * \brief
 * Implements gmx::analysismodules::Freevolume.
 *
 * \author Anatoly <titov_ai@pnpi.nrcki.ru>
 * \ingroup module_trajectoryanalysis
 */

#include "gromacs/options/ioptionscontainer.h"
#include "gromacs/math/do_fit.h"
#include "gromacs/topology/atomprop.h"
#include "gromacs/options/basicoptions.h"
#include "gromacs/selection/selectionoption.h"
#include "gromacs/trajectory/trajectoryframe.h"
#include "gromacs/trajectoryanalysis/analysissettings.h"
#include "gromacs/trajectoryanalysis/cmdlinerunner.h"
#include "gromacs/trajectoryanalysis/topologyinformation.h"
#include "gromacs/pbcutil/pbc.h"
#include "gromacs/math/vec.h"

#include <iostream>

class phaseGrid {
    private:
        std::vector< gmx::RVec >                            frame;      // координаты рассматриваемых атомов (берутся через std::move)
        std::vector< size_t >                               index;      // индексгруппы
        gmx::RVec                                           rx, ry, rz; // "базисные" вектора бокса
        const gmx::TopologyInformation                      *topology;  // топология
        t_pbc                                               *pbc;       // свойства бокса для фрейма
        std::vector< std::vector< std::vector< double > > > field;      // узлы сетки
        std::function< double(const gmx::TopologyInformation*,
                              t_pbc*, gmx::RVec cntr,
                              std::vector< std::pair< gmx::RVec*, size_t > >&) > fieldFunc; // шаблонная функция для подсчёта значений в узлах сетки
        class torGridMtrx {     // класс "переодичной" матрицы
            private:
                std::vector< std::vector< std::vector< std::vector< std::pair< gmx::RVec*, size_t > > > > > mtrx; // храним ссылки на атомы в радиусе узла
                size_t kx, ky, kz, n;    // честный размер матрицы и дополнительного слоя
                std::vector< std::tuple< int, int, int > > aphera; // список смещений для списка узлов в заданом радиусе от узла
                size_t pos(size_t a, size_t kDim) {  // функция преобразования координат добавочного слоя во внутрь матрицы
                    if (a < n) {        // если до матрицы
                        return kDim + a;
                    } else {            // если после матрицы
                        return a - kDim;
                    }
                }
            public:
                torGridMtrx(){}
                torGridMtrx(std::tuple< size_t, size_t, size_t > sz, size_t rad): kx {std::get< 0 >(sz)}, ky {std::get< 1 >(sz)}, kz {std::get< 2 >(sz)}, n{rad} {
                    mtrx.resize(std::get< 0 >(sz) + 2 * rad);  //sz - количество узлов по оси; rad - расстояние между узлами
                    std::vector< std::pair< gmx::RVec*, size_t > > a; // список пар (ссылка на РВек, его индекс)
                    a.resize(0);
                    for (auto &i : mtrx) {
                        i.resize(std::get< 1 >(sz) + 2 * rad);
                        for (auto &j : i) {
                            j.resize(std::get< 2 >(sz) + 2 * rad, a);
                        }
                    }
                    aphera.resize(0);
                    for (int ix {-static_cast< int >(n)}; ix <= static_cast< int >(n); ++ix) {
                        int lmt001 = std::floor(std::sqrt(static_cast< int >(n) * static_cast< int >(n) - ix * ix)); // считаем честно по радиусу от узла
                        for (int iy {-lmt001}; iy <= lmt001; ++iy) {    //радиус сечения по y
                            int lmt002 = std::floor(std::sqrt(lmt001 * lmt001 - iy * iy));  //диапазон по z - от одного узла (точка) до диаметра y, делённого на шаг узлов
                            for (int iz {-lmt002}; iz <= lmt002; ++iz) {
                                aphera.push_back(std::make_tuple(ix, iy, iz));
                            }
                        }
                    }
                }
                ~torGridMtrx() {}
                void psh(size_t x, size_t y, size_t z, std::pair< gmx::RVec*, size_t > value) { // перебор узлов в радиусе nSigma
                    x += n; y += n; z += n; //вносим поправку на добавленный слой
                    for (auto [ix, iy, iz] : aphera) {
                        mtrx[x + ix][y + iy][z + iz].push_back(value);
                    }
                }
                void makeShellDD(std::vector< size_t > &shll, size_t kDim) {
                    shll.resize(0);
                    shll.resize(2 * n);
                    std::generate(shll.begin(), shll.end(), [it = 0, nlim = n, klim = kDim]() mutable -> size_t { // создание индексов добавочного слоя
                        if (it < nlim) {
                            return it++;
                        } else {
                            return klim + it++;
                        }
                    });
                }
                void collapse() {   //сворачивает полученные слои внутрь матрицы
                    std::vector< size_t > shllXX, shllYY, shllZZ;
                    makeShellDD(shllXX, kx);
                    makeShellDD(shllYY, ky);
                    makeShellDD(shllZZ, kz);
                    for (const auto ix : shllXX) {
                        for (size_t iy {0}; iy < ky + 2 * n; ++iy) {
                            for (size_t iz {0}; iz < kz + 2 * n; ++iz) {
                                if (mtrx[ix][iy][iz].size() > 0) {
                                    mtrx[pos(ix, kx)][iy][iz].insert(mtrx[pos(ix, kx)][iy][iz].end(), mtrx[ix][iy][iz].begin(), mtrx[ix][iy][iz].end()); // сворачивание слоёв матрицы во внутрь
                                }
                            }
                        }
                    }
                    for (size_t ix {n}; ix < kx + n; ++ix) {
                        for (const auto iy : shllYY) {
                            for (size_t iz {0}; iz < kz + 2 * n; ++iz) {
                                if (mtrx[ix][iy][iz].size() > 0) {
                                    mtrx[ix][pos(iy, ky)][iz].insert(mtrx[ix][pos(iy, ky)][iz].end(), mtrx[ix][iy][iz].begin(), mtrx[ix][iy][iz].end()); // сворачивание слоёв матрицы во внутрь
                                }
                            }
                        }
                    }
                    for (size_t ix {n}; ix < kx + n; ++ix) {
                        for (size_t iy {n}; iy < ky + n; ++iy) {
                            for (const auto iz : shllZZ) {
                                if (mtrx[ix][iy][iz].size() > 0) {
                                    mtrx[ix][iy][pos(iz, kz)].insert(mtrx[ix][iy][pos(iz, kz)].end(), mtrx[ix][iy][iz].begin(), mtrx[ix][iy][iz].end()); // сворачивание слоёв матрицы во внутрь
                                }
                            }
                        }
                    }
                }
                std::vector< std::pair< gmx::RVec*, size_t > > gt(size_t x, size_t y, size_t z) { // возвращение списка РВеков для узла
                    return std::move(mtrx[x + n][y + n][z + n]);
                }
        };
    public:
        phaseGrid() {}
        ~phaseGrid() {}
        void updateTopology(const gmx::TopologyInformation &inpTop) {
            topology = &inpTop;
        }
        void updateIndex(std::vector< size_t > indx) {
            index = std::move(indx);
        }
        void updateBox(const matrix &inpBox) {
            rx[0] = inpBox[0][0] + 0.001; rx[1] = inpBox[0][1] + 0.001; rx[2] = inpBox[0][2] + 0.001;
            ry[0] = inpBox[1][0] + 0.001; ry[1] = inpBox[1][1] + 0.001; ry[2] = inpBox[1][2] + 0.001;
            rz[0] = inpBox[2][0] + 0.001; rz[1] = inpBox[2][1] + 0.001; rz[2] = inpBox[2][2] + 0.001;
        }
        void updateFrame(std::vector< gmx::RVec > &inpFrame) {
            frame = std::move(inpFrame);
        }
        void updatePBC(t_pbc *inpPBC) {
            pbc = inpPBC;
        }
        void updateFieldFunction(std::function< double(const gmx::TopologyInformation*, t_pbc*, gmx::RVec cntr, std::vector< std::pair< gmx::RVec*, size_t > >&) > &&inpFunc) {
            fieldFunc = std::move(inpFunc);
        }

        void constructField(std::tuple< size_t, size_t, size_t > grid /* number of points */, float radius /*nm*/) {
            double  dx {rx[XX] / static_cast< double >(std::get< 0 >(grid))},
                    dy {ry[YY] / static_cast< double >(std::get< 1 >(grid))},
                    dz {rz[ZZ] / static_cast< double >(std::get< 2 >(grid))}; // расстояние между узлами по осям
            torGridMtrx matras(grid, std::ceil(radius / std::max(std::max(dx, dy), dz)));
            for (size_t i {0}; i < frame.size(); ++i) {
                /*std::cout << std::get< 0 >(grid) << " " << std::get< 1 >(grid) << " " << std::get< 0 >(grid) << " | " <<
                             std::floor(frame[i][0] / dx) << " " << std::floor(frame[i][1] / dy) << " "  << std::floor(frame[i][2] / dz) << std::endl;*/
                matras.psh(std::floor(frame[i][0] / dx), std::floor(frame[i][1] / dy), std::floor(frame[i][2] / dz), std::make_pair(&frame[i], i)); // заполнение матрицы РВеками
            }
            matras.collapse();
            field.resize(std::get< 0 >(grid));
            #pragma omp parallel for ordered schedule(dynamic)
            for (size_t i = 0; i < std::get< 0 >(grid); ++i) { // подсчёт фазовой функции на сетке
                field[i].resize(std::get< 1 >(grid));
                for (size_t j {0}; j < std::get< 1 >(grid); ++j) {
                    field[i][j].resize(std::get< 2 >(grid), 0.);
                    for (size_t k {0}; k < std::get< 2 >(grid); ++k) {
                        std::vector< std::pair< gmx::RVec*, size_t > > tempRVecs = matras.gt(i, j, k);
                        if (tempRVecs.size() > 0) {
                            field[i][j][k] = fieldFunc(topology, pbc, {static_cast< float >(i * dx + dx / 2),
                                                                       static_cast< float >(j * dy + dy / 2),
                                                                       static_cast< float >(k * dz + dz / 2)}, tempRVecs);
                        }
                    }
                }
            }
            #pragma omp barrier
        }
        void getField(std::vector< std::vector< std::vector< double > > > &output) {
            output = std::move(field);
        }
        std::vector< std::vector< std::vector< double > > > moveField() {
            return std::move(field);
        }
};

void mergeFields(std::vector< std::vector< std::vector< long double > > > &a, const std::vector< std::vector< std::vector< double > > > &b) {
    for (size_t ix {0}; ix < b.size(); ++ix) {
        for (size_t iy {0}; iy < b[ix].size(); ++iy) {
            for (size_t iz {0}; iz < b[ix][iy].size(); ++iz) {
                a[ix][iy][iz] += b[ix][iy][iz];
            }
        }
    }
}

void normField(std::vector< std::vector< std::vector< long double > > > &grd, long double factor, gmx::RVec voxel_) {
    long double grdSum_ = 0.;
    // summ grid
    for (size_t ix {0}; ix < grd.size(); ++ix) {
        for (size_t iy {0}; iy < grd[ix].size(); ++iy) {
            for (size_t iz {0}; iz < grd[ix][iy].size(); ++iz) {
                grdSum_ += grd[ix][iy][iz]*voxel_[XX]*voxel_[YY]*voxel_[ZZ];
            }
        }
    }
    // normalize it
    for (size_t ix {0}; ix < grd.size(); ++ix) {
        for (size_t iy {0}; iy < grd[ix].size(); ++iy) {
            for (size_t iz {0}; iz < grd[ix][iy].size(); ++iz) {
                grd[ix][iy][iz] *= (factor / grdSum_);
            }
        }
    }
}

//! Normalization for the computed density grid.
enum class DensityType : int {
    Probability,
    Mass,
    Charge,
    ElectronDensity,
    Count
};
//! String values corresponding to Normalization.
const gmx::EnumerationArray<DensityType, const char*> densityTypeNames = {
    { "probability", "mass", "charge", "electrons" }
};

//! Normalization for the computed density grid.
enum class SigmaType : int {
    Constant,
    VdW,
    Count
};
//! String values corresponding to Normalization.
const gmx::EnumerationArray<SigmaType, const char*> sigmaTypeNames = {
    { "constant", "vdw" }
};

/*! \brief
 * \ingroup module_trajectoryanalysis
 */
class denseGrid : public gmx::TrajectoryAnalysisModule
{
    public:

        denseGrid();
        virtual ~denseGrid();

        //! Set the options and setting
        virtual void initOptions(gmx::IOptionsContainer          *options,
                                 gmx::TrajectoryAnalysisSettings *settings);

        //! First routine called by the analysis framework
        virtual void initAnalysis(const gmx::TrajectoryAnalysisSettings &settings,
                                  const gmx::TopologyInformation        &top);

        virtual void initAfterFirstFrame(const gmx::TrajectoryAnalysisSettings &settings,
                                         const t_trxframe &fr);

        //! Call for each frame of the trajectory
        virtual void analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc,
                                  gmx::TrajectoryAnalysisModuleData *pdata);

        //! Last routine called by the analysis framework
        virtual void finishAnalysis(int nframes);

        //! Routine to write output, that is additional over the built-in
        virtual void writeOutput();

    private:
        // Copy and assign disallowed by base. */
        gmx::Selection              sel_;

        gmx::Selection              fitGroupSel_;
        std::vector< real >         fittedIndexes;

        std::vector< size_t >       index;
        std::vector< double >       densFactors_;
        std::vector< real >         sigmaFactors_;
        phaseGrid                   grid;
        std::tuple< size_t, size_t, size_t >                        gridDim;
        std::vector< std::vector< std::vector< long double > > >    finalRest;
        double                      nFactor_;
        float                       sideX {0.}, sideY {0.}, sideZ {0.};
        float                       sideXX {0.}, sideYY {0.}, sideZZ {0.};
        float                       sensibleRad_ {0.1};

        float                       gridStep_   {0.1};
        DensityType                 denstype_   {DensityType::Probability};
        SigmaType                   sigmatype_  {SigmaType::Constant};
        double                      sigma_      {0.1};
        int                         nSigma_     {3};
        std::string                 outGridFnm  {"densgrid.dx"};
        bool                        normalisation_ {false};
        bool                        fitting_    {false};
        bool                        fillSolv_   {false};
        std::vector< gmx::RVec >    fitGroup;
        gmx::RVec                   boxCenterDiv;
};

denseGrid::denseGrid(): TrajectoryAnalysisModule()
{
}

denseGrid::~denseGrid()
{
}

void
denseGrid::initOptions( gmx::IOptionsContainer          *options,
                        gmx::TrajectoryAnalysisSettings *settings)
{
    static const char *const desc[] = {
        "[THISMODULE] to be done"
        "the trajectory must be preeditted with gmx trjconv -centered"
        "if its just centered then either:"
        "no flags or -fitted -fillSolv"
        "if its also -fit rot+trans"
        "then:"
        "just -fitted"
        ""
        ""
        ""
        ""
        ""
    };
    // Add the descriptive text (program help text) to the options
    settings->setHelpText(desc);
    // Add option for selection
    options->addOption(gmx::SelectionOption("sel")
                                .store(&sel_)
                                .required()
                                .description("select group for density"));
    // Add option for fitted selection
    options->addOption(gmx::SelectionOption("fitSel")
                                .store(&fitGroupSel_)
                                .description("select fitted group"));
    // Add option for distance between grid nodes
    options->addOption(gmx::FloatOption("gs")
                            .store(&gridStep_).defaultValue(0.1)
                            .description("distance between grid nodes"));
    // Add option for density constant(s)
    options->addOption(gmx::EnumOption<DensityType>("denstype")
                            .enumValue(densityTypeNames).defaultValue(DensityType::Probability)
                            .store(&denstype_)
                            .description("Type of computed density"));
    // Add option for sigma value
    options->addOption(gmx::DoubleOption("sigma")
                            .store(&sigma_).defaultValue(0.1)
                            .description("sigma value for function (nm)"));
    // Add option for sensetivity radius
    options->addOption(gmx::IntegerOption("nSigma")
                            .store(&nSigma_).defaultValue(3)
                            .description("number of sigma values for summation"));
    // Add option for output file name
    options->addOption(gmx::StringOption("grid")
                            .store(&outGridFnm).defaultValue("densgrid.dx")
                            .description("output filename for format: OpenDX portable grid."));
    // Add option for normalisation procedure
    options->addOption(gmx::BooleanOption("norm")
                            .store(&normalisation_).defaultValue(true)
                            .description("flag for switching on/off normalisation of data"));
    // Add option for fitting procedure
    options->addOption(gmx::BooleanOption("fitted")
                            .store(&fitting_).defaultValue(false)
                            .description("flag for selecting if input trajectory is to be fitted"));
    // Add option for extra fitting procedure

    /*
     *
     * need to add a property
     * that fillSol requires
     * fitSel flag as well
     *
     */

    options->addOption(gmx::BooleanOption("fillSol")
                            .store(&fillSolv_).defaultValue(false)
                            .description("flag for selecting if output data for fitted choice should be expanded to full box with solv"));
    // Add option for nSigma change
    options->addOption(gmx::EnumOption<SigmaType>("smgtype")
                            .enumValue(sigmaTypeNames).defaultValue(SigmaType::Constant)
                            .store(&sigmatype_)
                            .description("Type of considered sigma"));
    // Control input settings
    settings->setFlags(gmx::TrajectoryAnalysisSettings::efNoUserPBC);
    settings->setFlag(gmx::TrajectoryAnalysisSettings::efUseTopX);
    settings->setFlag(gmx::TrajectoryAnalysisSettings::efRequireTop);
    settings->setPBC(true);
}

void
denseGrid::initAnalysis(    const gmx::TrajectoryAnalysisSettings   &settings,
                            const gmx::TopologyInformation          &top)
{
    std::cout << "initAnalysis started" << std::endl;
    index.resize(0); // считывание главной группы
    densFactors_.resize(0);
    sigmaFactors_.resize(0);

    std::vector< real > fitted;
    fitted.resize(0);

    AtomProperties aps;
    real sigmaTemp {0.1};
    for (gmx::ArrayRef< const int >::iterator ai {sel_.atomIndices().begin()}; (ai < sel_.atomIndices().end()); ++ai) {
        index.push_back(static_cast< size_t >(*ai));
        fitted.push_back(0.);
        switch (denstype_) {
            case DensityType::Probability :
                densFactors_.push_back(1.);
                break;
            case DensityType::Charge :
                densFactors_.push_back(top.atoms()->atom[static_cast< size_t >(*ai)].q);
                break;
            case DensityType::Mass :
                densFactors_.push_back(top.atoms()->atom[static_cast< size_t >(*ai)].m);
                break;
            case DensityType::ElectronDensity :
                densFactors_.push_back(top.atoms()->atom[static_cast< size_t >(*ai)].atomnumber);
                break;
        }

        switch (sigmatype_) {
            case SigmaType::Constant :
                sensibleRad_ = sigma_;
                sigmaFactors_.push_back(sigma_);
                break;
            case SigmaType::VdW :
                aps.setAtomProperty(epropVDW, *(top.atoms()->resinfo[static_cast< size_t >(*ai)].name), *(top.atoms()->atomname[static_cast< size_t >(*ai)]), &sigmaTemp);
                sigmaFactors_.push_back(sigmaTemp);
                sensibleRad_ = std::max(sensibleRad_, sigmaFactors_.back());
                break;
        }
    }

    sensibleRad_ *= nSigma_;

    if (fillSolv_) {
        fittedIndexes.resize(0);
        for (gmx::ArrayRef< const int >::iterator ai {fitGroupSel_.atomIndices().begin()}; (ai < fitGroupSel_.atomIndices().end()); ++ai) {
            fitted[static_cast< size_t >(*ai)] = 1.;
        }
        fitted.push_back(0.);
        fitted.push_back(0.);
        fitted.push_back(0.);
        fitted.push_back(0.);
        fittedIndexes = std::move(fitted);
    }

    grid.updateTopology(top);
    grid.updateIndex(index);
    grid.updateFieldFunction([sgm = sigmaFactors_, densf = densFactors_]( const gmx::TopologyInformation* ti,
                                            t_pbc* t_pbc, gmx::RVec cntr,
                                            std::vector< std::pair< gmx::RVec*, size_t > > &rvecs) mutable -> double {
        double out {0.};
        gmx::RVec temp;
        for(size_t i {0}; i < rvecs.size(); ++i) {
            pbc_dx(t_pbc, cntr, *rvecs[i].first, temp);
            out += (densf[rvecs[i].second] * std::exp(-temp.norm2()/(2 * sgm[rvecs[i].second] * sgm[rvecs[i].second])) / (sgm[rvecs[i].second] * sgm[rvecs[i].second] * sgm[rvecs[i].second] * std::pow(2 * M_PI, 3./2.)));
        }
        return out;
    });

    boxCenterDiv = {0., 0., 0.};

    std::cout << "initAnalysis finished" << std::endl;
}

void
denseGrid::initAfterFirstFrame( const gmx::TrajectoryAnalysisSettings &settings,
                                const t_trxframe &fr)
{
    std::cout << "initAfterFirstFrame started" << std::endl;
    if (fitting_) {
        if (fillSolv_) {
            fitGroup.resize(0);
            for (size_t i {0}; i < index.size(); ++i) {
                fitGroup.push_back(fr.x[index[i]]);
            }
            fitGroup.push_back({0., 0., 0.});
            fitGroup.push_back({0., 0., 0.});
            fitGroup.push_back({0., 0., 0.});
            fitGroup.push_back({0., 0., 0.});
            gridDim = {std::ceil(fr.box[XX][XX] / gridStep_), std::ceil(fr.box[YY][YY] / gridStep_), std::ceil(fr.box[ZZ][ZZ] / gridStep_)};
        } else {
            float   a {static_cast<float>(std::sqrt(std::pow(std::abs(fr.box[XX][XX]) + std::abs(fr.box[YY][XX]) + std::abs(fr.box[ZZ][XX]), 2)))},
                    b {static_cast<float>(std::sqrt(std::pow(std::abs(fr.box[XX][YY]) + std::abs(fr.box[YY][YY]) + std::abs(fr.box[ZZ][YY]), 2)))},
                    c {static_cast<float>(std::sqrt(std::pow(std::abs(fr.box[XX][ZZ]) + std::abs(fr.box[YY][ZZ]) + std::abs(fr.box[ZZ][ZZ]), 2)))},
                    mainDiag {std::sqrt(a * a + b * b + c * c)};
            gridDim = {std::ceil(mainDiag / gridStep_), std::ceil(mainDiag / gridStep_), std::ceil(mainDiag / gridStep_)};
        }
    } else {
        gridDim = {std::ceil(fr.box[XX][XX] / gridStep_), std::ceil(fr.box[YY][YY] / gridStep_), std::ceil(fr.box[ZZ][ZZ] / gridStep_)};
    }
    finalRest.resize(0);
    finalRest.resize(std::get< 0 >(gridDim));
    for (size_t ix {0}; ix < std::get< 0 >(gridDim); ++ix) {
        finalRest[ix].resize(std::get< 1 >(gridDim));
        for (size_t iy {0}; iy < std::get< 1 >(gridDim); ++iy) {
            finalRest[ix][iy].resize(std::get< 2 >(gridDim), 0.);
        }
    }
    std::cout << "initAfterFirstFrame finished" << std::endl;
}

gmx::RVec rv_to_RV(const rvec a) {
    gmx::RVec temp;
    temp[XX] = a[XX];
    temp[YY] = a[YY];
    temp[ZZ] = a[ZZ];
    return temp;
}

void coordQuizXYZ (real &ri, real di) {
    if (ri < 0) {
        while (ri < 0) {
            ri += di;
        }
    } else if (ri > di) {
        while (ri > di) {
            ri -= di;
        }
    }
}

void coordQuiz (const matrix box, std::vector< gmx::RVec > &trj) {
    #pragma omp parallel for schedule(dynamic) // вписываем все координаты в прямоугольник
    for (size_t i = 0; i < trj.size(); ++i) {
        coordQuizXYZ(trj[i][XX], box[XX][XX]);
        coordQuizXYZ(trj[i][YY], box[YY][YY]);
        coordQuizXYZ(trj[i][ZZ], box[ZZ][ZZ]);
    }
    #pragma omp barrier
}

void
denseGrid::analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc, gmx::TrajectoryAnalysisModuleData *pdata)
{
    std::cout << "analyzeFrame started" << std::endl;
    std::vector< gmx::RVec > trajectoryFrame;
    trajectoryFrame.resize(0);
    // считывания текущего фрейма траектории
    for (size_t i {0}; i < index.size(); ++i) {
        trajectoryFrame.push_back(fr.x[index[i]]);
    }

    if (fitting_) {
        if (fillSolv_) {
            trajectoryFrame.push_back({0., 0., 0.});
            trajectoryFrame.push_back({fr.box[XX][XX], fr.box[XX][YY], fr.box[XX][ZZ]});
            trajectoryFrame.push_back({fr.box[YY][XX], fr.box[YY][YY], fr.box[YY][ZZ]});
            trajectoryFrame.push_back({fr.box[ZZ][XX], fr.box[ZZ][YY], fr.box[ZZ][ZZ]});
            rvec* temp;
            snew(temp, trajectoryFrame.size() + 4);
            for (size_t i {0}; i < trajectoryFrame.size() + 4; ++i) {
                copy_rvec(trajectoryFrame[i].as_vec(), temp[i]);
            }
            trajectoryFrame.resize(0);
            do_fit(index.size() + 4, fittedIndexes.data(), as_rvec_array(fitGroup.data()), temp);
            gmx::RVec tempR, x, y, z;
            x = rv_to_RV(temp[index.size() + 1]) - rv_to_RV(temp[index.size()]);
            y = rv_to_RV(temp[index.size() + 2]) - rv_to_RV(temp[index.size()]);
            z = rv_to_RV(temp[index.size() + 3]) - rv_to_RV(temp[index.size()]);
            std::vector< size_t > tempIndex;
            tempIndex.resize(0);
            for (size_t i {0}; i < index.size(); ++i) {
                for (int ix {-1}; ix < 2; ++ix) {
                    for (int iy {-1}; iy < 2; ++iy) {
                        for (int iz {-1}; iz < 2; ++iz) {
                            tempR = rv_to_RV(temp[i]) + static_cast< real >(ix) * x + static_cast< real >(iy) * y + static_cast< real >(iz) * z;
                            if ((tempR[XX] > 0) && (tempR[XX] < fr.box[XX][XX]) && (tempR[YY] > 0) && (tempR[YY] < fr.box[YY][YY]) && (tempR[ZZ] > 0) && (tempR[ZZ] < fr.box[ZZ][ZZ])) {
                                trajectoryFrame.push_back(tempR);
                                tempIndex.push_back(index[i]);
                            }
                        }
                    }
                }
            }
            sfree(temp);
            grid.updateIndex(tempIndex);
            grid.updateBox(fr.box);
        } else {
            float   a {static_cast<float>(std::sqrt(std::pow(std::abs(fr.box[XX][XX]) + std::abs(fr.box[YY][XX]) + std::abs(fr.box[ZZ][XX]), 2)))},
                    b {static_cast<float>(std::sqrt(std::pow(std::abs(fr.box[XX][YY]) + std::abs(fr.box[YY][YY]) + std::abs(fr.box[ZZ][YY]), 2)))},
                    c {static_cast<float>(std::sqrt(std::pow(std::abs(fr.box[XX][ZZ]) + std::abs(fr.box[YY][ZZ]) + std::abs(fr.box[ZZ][ZZ]), 2)))},
                    mainDiag {std::sqrt(a * a + b * b + c * c)};
            gmx::RVec   oldMid {(fr.box[XX][XX] + fr.box[YY][XX] + fr.box[ZZ][XX]) / 2, (fr.box[XX][YY] + fr.box[YY][YY] + fr.box[ZZ][YY]) / 2, (fr.box[XX][ZZ] + fr.box[YY][ZZ] + fr.box[ZZ][ZZ]) / 2},
                        newMid {mainDiag / 2, mainDiag / 2, mainDiag / 2},
                        shift {newMid - oldMid};
            #pragma omp parallel for schedule(dynamic) // вписываем все координаты в прямоугольник
            for (size_t i = 0; i < trajectoryFrame.size(); ++i) {
                trajectoryFrame[i] += shift;
            }
            #pragma omp barrier
            matrix newBox {{mainDiag, 0., 0.}, {0., mainDiag, 0.}, {0., 0., mainDiag}};
            coordQuiz(newBox, trajectoryFrame);
            grid.updateBox(newBox);
            boxCenterDiv[XX] = (fr.box[XX][XX] + fr.box[YY][XX] + fr.box[ZZ][XX] - mainDiag) / 2;
            boxCenterDiv[YY] = (fr.box[XX][YY] + fr.box[YY][YY] + fr.box[ZZ][YY] - mainDiag) / 2;
            boxCenterDiv[ZZ] = (fr.box[XX][ZZ] + fr.box[YY][ZZ] + fr.box[ZZ][ZZ] - mainDiag) / 2;
        }
    } else {
        grid.updateBox(fr.box);
        coordQuiz(fr.box, trajectoryFrame);
    }

    grid.updatePBC(pbc);
    grid.updateFrame(trajectoryFrame);
    grid.constructField(gridDim, sensibleRad_);

    mergeFields(finalRest, grid.moveField()); // набираем статистику поля

    sideX += fr.box[XX][XX];
    sideY += fr.box[YY][YY];
    sideZ += fr.box[ZZ][ZZ];

    sideXX += fr.box[XX][XX] + fr.box[YY][XX] + fr.box[ZZ][XX];
    sideYY += fr.box[XX][YY] + fr.box[YY][YY] + fr.box[ZZ][YY];
    sideZZ += fr.box[XX][ZZ] + fr.box[YY][ZZ] + fr.box[ZZ][ZZ];

    std::cout << "frame: " << frnr << " done" << std::endl;
}

void printDX(std::vector< std::vector< std::vector< long double > > > &prnt,
             std::string fName, std::tuple< size_t, size_t, size_t > nodeCount,
             gmx::RVec origin, std::tuple< float, float, float > sideDist,
             std::function< void(const std::vector< std::vector< std::vector< long double > > >&,
                                 std::string) > printData) { // функция вывода файла в формате VMD .dx
    float nm2a {10.};
    FILE *fileOut;
    fileOut = fopen(fName.c_str(), "w");
    fprintf(fileOut, "object 1 class gridpositions counts %lu %lu %lu\n", std::get< 0 >(nodeCount), std::get< 1 >(nodeCount), std::get< 2 >(nodeCount));
    fprintf(fileOut, "origin %1.6f %1.6f %1.6f\n", origin[XX] * nm2a, origin[YY] * nm2a, origin[ZZ] * nm2a);
    fprintf(fileOut, "delta %3.6f %3.6f %3.6f\n", std::get< 0 >(sideDist) / std::get< 0 >(nodeCount) * nm2a, 0.0, 0.0);
    fprintf(fileOut, "delta %3.6f %3.6f %3.6f\n", 0.0, std::get< 1 >(sideDist) / std::get< 1 >(nodeCount) * nm2a, 0.0);
    fprintf(fileOut, "delta %3.6f %3.6f %3.6f\n", 0.0, 0.0, std::get< 2 >(sideDist) / std::get< 2 >(nodeCount) * nm2a);
    fprintf(fileOut, "object 2 class gridconnections counts  %lu %lu %lu\n", std::get< 0 >(nodeCount), std::get< 1 >(nodeCount), std::get< 2 >(nodeCount));
    fprintf(fileOut, "object 3 class array type \"double\" rank 0 items %lu data follows\n", std::get< 0 >(nodeCount) * std::get< 1 >(nodeCount) * std::get< 2 >(nodeCount));
    fclose(fileOut);

    printData(prnt, fName); // внешняя функция вывода для конкретной структуры данных

    fileOut = fopen(fName.c_str(), "a");
    fprintf(fileOut, "attribute \"dep\" string \"positions\"\n");
    fprintf(fileOut, "object \"density\" class field\n");
    fprintf(fileOut, "component \"positions\" value 1\n");
    fprintf(fileOut, "component \"connections\" value 2\n");
    fprintf(fileOut, "component \"data\" value 3\n");
    fclose(fileOut);
}

void
denseGrid::finishAnalysis(int nframes)
{
    std::cout << "\nfinishAnalysis started" << std::endl;
    sideX /= static_cast< float >(nframes); // вычисляем средние размеры бокса
    sideY /= static_cast< float >(nframes);
    sideZ /= static_cast< float >(nframes);
    //boxCenterDiv /= static_cast< float >(nframes);
    sideXX /= static_cast< float >(nframes); // вычисляем средние размеры бокса
    sideYY /= static_cast< float >(nframes);
    sideZZ /= static_cast< float >(nframes);


    #pragma omp parallel for schedule(dynamic) // усреднение значений в узлах матрицы и вычитание раствора
    for (size_t ix = 0; ix < finalRest.size(); ++ix) {
        for (size_t iy {0}; iy < finalRest[ix].size(); ++iy) {
            for (size_t iz {0}; iz < finalRest[ix][iy].size(); ++iz) {
                finalRest[ix][iy][iz] = finalRest[ix][iy][iz] / static_cast< double >(nframes);
            }
        }
    }
    #pragma omp barrier

    if (normalisation_) {
        gmx::RVec voxel_(sideX / static_cast< float >(std::get< 0 >(gridDim)), sideY / static_cast< float >(std::get< 1 >(gridDim)), sideZ / static_cast< float >(std::get< 2 >(gridDim)));
        double densSum_ = 0;
        for (auto i = 0; i < densFactors_.size(); ++i) {
            densSum_ += densFactors_[i];
        }
        normField(finalRest, densSum_, voxel_);
    }

    auto FFF = [](const std::vector< std::vector< std::vector< long double > > > &prnt, std::string fName) -> void { // вывод в файл данных
        FILE *fileOutL;
        fileOutL = fopen(fName.c_str(), "a");
        for (size_t ix {0}; ix < prnt.size(); ++ix) {
            for (size_t iy {0}; iy < prnt[ix].size(); ++iy) {
                for (size_t iz {0}; iz < prnt[ix][iy].size(); ++iz) {
                    fprintf(fileOutL, "%3.8Lf ", prnt[ix][iy][iz]);
                    if ((ix * prnt.front().size() * prnt.front().front().size() + iy * prnt.front().front().size() + iz) % 3 == 2) {
                        fprintf(fileOutL, "\n");
                    }
                }
            }
        }
        if (prnt.size() * prnt.front().size() * prnt.front().front().size() % 3 != 0) {
            fprintf(fileOutL, "\n");
        }
        fclose(fileOutL);
    };

    if (fitting_) {
        if (fillSolv_) {
            printDX(finalRest, outGridFnm, gridDim, {sideX / std::get< 0 >(gridDim) / 2, sideY / std::get< 1 >(gridDim) / 2, sideZ / std::get< 2 >(gridDim) / 2}, std::make_tuple(sideX, sideY, sideZ), FFF);
        } else {
            float r {std::min(sideX, std::min(sideY, sideZ)) / 2};
            float mainDiag {std::sqrt(sideXX * sideXX + sideYY * sideYY + sideZZ * sideZZ)};
            gmx::DVec ri, rmid {mainDiag / 2, mainDiag / 2, mainDiag / 2};
            for (size_t ix {0}; ix < finalRest.size(); ++ix) {
                for (size_t iy {0}; iy < finalRest[ix].size(); ++iy) {
                    for (size_t iz {0}; iz < finalRest[ix][iy].size(); ++iz) {
                        ri = {mainDiag * ((static_cast< float >(ix) + 0.5) / static_cast< float >(std::get< 0 >(gridDim))),
                              mainDiag * ((static_cast< float >(iy) + 0.5) / static_cast< float >(std::get< 1 >(gridDim))),
                              mainDiag * ((static_cast< float >(iz) + 0.5) / static_cast< float >(std::get< 2 >(gridDim)))};
                        if ((ri - rmid).norm() > r) {
                            finalRest[ix][iy][iz] = 0.;
                        }
                    }
                }
            }
            printDX(finalRest, outGridFnm, gridDim, boxCenterDiv /*{sideX / std::get< 0 >(gridDim) / 2 + boxCenterDiv[XX],
                                                     /*sideY / std::get< 1 >(gridDim) / 2 + boxCenterDiv[YY],
                                                     /*sideZ / std::get< 2 >(gridDim) / 2 + boxCenterDiv[ZZ]}*/, std::make_tuple(mainDiag, mainDiag, mainDiag), FFF);
        }
    } else {
        printDX(finalRest, outGridFnm, gridDim, {sideX / std::get< 0 >(gridDim) / 2, sideY / std::get< 1 >(gridDim) / 2, sideZ / std::get< 2 >(gridDim) / 2}, std::make_tuple(sideX, sideY, sideZ), FFF);
    }
    std::cout << "finishAnalysis finished" << std::endl;
}

void
denseGrid::writeOutput() {}

/*! \brief
 * The main function for the analysis template.
 */
int
main(int argc, char *argv[])
{
    return gmx::TrajectoryAnalysisCommandLineRunner::runAsMain<denseGrid>(argc, argv);
}
